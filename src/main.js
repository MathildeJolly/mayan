import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import BaseButton from './components/global/BaseButton'
import NavBar from './components/global/NavBar'
import store from './store.js'
import "tailwindcss/tailwind.css"

const app = createApp(App)

app.component('base-button', BaseButton)
app.component('nav-bar', NavBar)
app.use(router)
app.use(store)
app.mount('#app')
