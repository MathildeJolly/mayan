import { createWebHistory, createRouter } from "vue-router";
import Home from "@/views/Home.vue";
import Users from "@/views/Users.vue";
import Profile from "@/views/Profile.vue";
import User from "@/views/User.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/users",
    name: "Users",
    component: Users,
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
  },
  {
    path: '/users/:userId',
    name: 'User',
    component: User
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;